var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SchemaOptions = {
  collection: 'comments',
  versionKey: 'version',
  timestamps: true
};

var SchemaProperties = {
  article:{
    type: Schema.Types.ObjectId,
    ref:'comments'
  },
  content:{
    required: true,
    type: String,
    maxlength: 500
  },
  author:{
    required: true,
    type: String
  },
  comments:{
    type: Array,
    default: []
  }
};
var commentsSchema = new Schema(SchemaProperties, SchemaOptions);

module.exports = mongoose.model('Comments', commentsSchema);
