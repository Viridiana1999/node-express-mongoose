$(document).ready(function() {
  var getArticlesCall = {
    url: 'http://localhost/articles',
    dataType: 'json',
    method: 'Get'
  };

  $.ajax(getArticlesCall)
  .done(printArticles)
  .fail(logearMensaje)
  .always(notificarUsuario)

  function printArticles(response) {
    var post = $('#article__empty')
      .clone()
      .removeAttr('id')
      .attr('id', article.id)

    $(post)
    .find('.article__content')
      .text(article.content)
      .end()
    .find('.article__title')
      .text(article.title)
      .end()
    .find('.article__autor')
      .text(article.autor)
      .end()
    .find('.article__link')
      .attr('href', article.link)
      .end();
  }

  function logearMensaje(msg) {
    console.log(msg);
  }
});
