var router = require('express').Router();
var articlesController = require('./controllers/articles');
var commentsController = require('./controllers/comments');
router.get('/articles', articlesController.readArticles);
router.post('/articles', articlesController.createArticle);

router.get('/articles/:id', articlesController.readOneArticle);

router.put('/articles/:id', articlesController.updateOneArticle);

router.delete('/articles/:id', articlesController.updateOneArticle);


router.get('/comments', commentsController.readComments);
router.post('/comments', commentsController.createComments);
router.get('/comments/:id', commentsController.readOneComment);

router.put('/comments/:id', commentsController.updateOneComments);

router.delete('/comments/:id', commentsController.updateOneComments);


module.exports = router;
